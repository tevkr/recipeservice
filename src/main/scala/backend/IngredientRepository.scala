package dev.yanovsky
package backend

import scala.collection.mutable.ListBuffer
import models.Ingredient

trait IngredientRepository {
  def getAll(): Either[String, Seq[Ingredient]]
  def add(ingredient: Ingredient): Either[String, Unit]
  def deleteByName(ingredientName: String): Either[String, Unit]
}