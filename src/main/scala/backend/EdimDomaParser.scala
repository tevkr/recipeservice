package dev.yanovsky
package backend

import models.{Ingredient, Recipe}

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import io.circe.generic.auto.*
import sttp.model.Uri
import sttp.client3.*
import sttp.client3.circe.*

import java.net.http.HttpClient
import scala.collection.mutable.ListBuffer

class EdimDomaParser(backend: SttpBackend[sttp.client3.Identity, Any]) extends Parser{
  private val baseUrl = "https://www.edimdoma.ru/retsepty"

  def getIngredients(partOfIngredientName: String): Either[String, Seq[Ingredient]] = {
    val request = basicRequest
      .get(uri"$baseUrl/ingredients/filter?q=$partOfIngredientName")
      .response(asJson[Seq[Ingredient]])
    val response = request.send(backend)
    response.body match {
      case Left(error) => Left(error.getMessage)
      case Right(ingredients) => ingredients.size match {
        case 0 => Left("Нет подходящих ингредиентов")
        case _ => Right(ingredients)
      }
    }
  }

  def getRecipes(ingredients: Seq[Ingredient]): Either[String, Seq[Recipe]] = {
    val ingredientsQueryString = ingredients.map(ingr => s"with_ingredient[]=${ingr.id}").mkString("&")
    val request = basicRequest.get(uri"$baseUrl?$ingredientsQueryString".querySegmentsEncoding(Uri.QuerySegmentEncoding.RelaxedWithBrackets))
    val response = request.send(backend)
    response.body match {
      case Left(error) => Left(error)
      case Right(html) => Right(parseRecipes(html))
    }
  }

  private def parseRecipes(html: String): Seq[Recipe] = {
    val recipes = ListBuffer.empty[Recipe]
    val doc = Jsoup.parse(html)
    val cards = doc.select(".card:not(.card_commercial)")
    cards.forEach { card =>
      if (card.hasClass("card_wide")) {
        val aElement = card.select(".card__content").select("a")
        val path = aElement.attr("href").substring(10)
        val name =  aElement.select(".title").text()
        recipes.addOne(Recipe(path, name))
      }
      else if (card.hasClass("card-recipe-special")) {
        val aElement = card.select("a")
        val path = aElement.attr("href").substring(10)
        val name =  aElement.select(".title").text()
        recipes.addOne(Recipe(path, name))
      }
      else {
        val aElement = card.select(".card__content").select(".card__description").select("a")
        val path = aElement.attr("href").substring(10)
        val name =  aElement.select(".title").text()
        recipes.addOne(Recipe(path, name))
      }
    }
    recipes.toSeq
  }

  def getSteps(recipe: Recipe): Either[String, Seq[String]] = {
    val request = basicRequest.get(uri"$baseUrl/${recipe.path}")
    val response = request.send(backend)
    response.body match {
      case Left(error) => Left(error)
      case Right(html) => Right(parseSteps(html))
    }
  }

  private def parseSteps(html: String): Seq[String] = {
    val recipeSteps = ListBuffer.empty[String]
    val doc = Jsoup.parse(html)
    val steps = doc.select("div[data-module=step_hint]")
    steps.forEach { step => recipeSteps.addOne(step.text())}
    recipeSteps.toSeq
  }
}
