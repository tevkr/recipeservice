package dev.yanovsky
package backend

import models.Ingredient

import scala.collection.mutable.ListBuffer

class InMemoryIngredientRepository extends IngredientRepository {
  private val ingredients = ListBuffer.empty[Ingredient]

  def getAll(): Either[String, Seq[Ingredient]] = {
    if (ingredients.isEmpty) {
      Left("Нет ингредиентов")
    } else {
      Right(ingredients.toSeq)
    }
  }

  def add(ingredient: Ingredient): Either[String, Unit] = {
    if (ingredients.exists(_.name == ingredient.name)) {
      Left(s"Ингредиент с именем ${ingredient.name} уже существует")
    } else {
      ingredients += ingredient
      Right(())
    }
  }

  def deleteByName(ingredientName: String): Either[String, Unit] = {
    ingredients.find(_.name == ingredientName) match {
      case Some(ingredient) =>
        ingredients -= ingredient
        Right(())
      case None => Left(s"Ингредиент с именем $ingredientName не найден")
    }
  }
}