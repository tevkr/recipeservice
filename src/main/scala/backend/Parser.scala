package dev.yanovsky
package backend

import models.{Ingredient, Recipe}

trait Parser {
  def getIngredients(partOfIngredientName: String): Either[String, Seq[Ingredient]]
  def getRecipes(ingredients: Seq[Ingredient]): Either[String, Seq[Recipe]]
  def getSteps(recipe: Recipe): Either[String, Seq[String]]
}