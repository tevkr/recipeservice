package dev.yanovsky

import scala.io.StdIn
import backend.{EdimDomaParser, InMemoryIngredientRepository}

import dev.yanovsky.frontend.ConsoleFrontend
import sttp.client3.HttpClientSyncBackend

object Main {
  def main(args: Array[String]): Unit = {
    new ConsoleFrontend(
      new InMemoryIngredientRepository(),
      new EdimDomaParser(HttpClientSyncBackend())
    ).start()
  }
}