package dev.yanovsky
package frontend

import backend.{IngredientRepository, Parser}
import models.Ingredient
import scala.io.StdIn.readLine

class ConsoleFrontend(ingredientsRepo: IngredientRepository, parser: Parser) {
  private def printIngredients(): Unit = {
    println("Добавленные ингредиенты:")
    ingredientsRepo.getAll() match {
      case Left(message) => println(message)
      case Right(ingrs) => println(ingrs.map(ingr => ingr.name).mkString("; "))
    }
  }

  private def printMenu(): Unit = {
    println("=" * 25)
    printIngredients()
    println("Меню:")
    println("1) Добавить ингредиент")
    println("2) Удалить ингредиент")
    println("3) Найти рецепт")
    println("q) Выйти")
  }

  private def addIngredient(): Unit = {
    val partOfIngredientName = readLine("Введите часть названия ингредиента: ")
    parser.getIngredients(partOfIngredientName) match {
      case Right(ingredients) =>
        println("Подходящие ингредиенты: ")
        ingredients.foreach(r => println(s"- ${r.name}"))
        val ingredientName = readLine("Введите полное название ингредиента: ")
        val ingredient = ingredients.find(_.name == ingredientName.trim())
        ingredient match {
          case Some(i) => ingredientsRepo.add(i) match {
            case Right(_) => println("Ингредиент успешно добавлен")
            case Left(error) => println(s"Ошибка при добавлении ингредиента: $error")
          }
          case None => println("Ингредиент не найден")
        }
      case Left(error) => println(s"Ошибка при добавлении ингредиента: $error")
    }
  }

  private def removeIngredient(): Unit = {
    val ingredientName = readLine("Введите полное название ингредиента: ")
    ingredientsRepo.deleteByName(ingredientName.trim()) match {
      case Right(_) => println("Ингредиент успешно удален")
      case Left(error) => println(s"Ошибка при удалении ингредиента: $error")
    }
  }

  private def getRecipe(): Unit = {
    val allIngredients = ingredientsRepo.getAll() match {
      case Right(ingredients) => ingredients
      case Left(error) => println(s"Ошибка при поиске рецепта: $error"); return
    }
    parser.getRecipes(allIngredients) match {
      case Right(recipes) =>
        println("Подходящие рецепты: ")
        recipes.foreach(r => println(s"- ${r.name}"))
        val recipeName = readLine("Введите полное название рецепта: ")
        val recipe = recipes.find(_.name == recipeName.trim())
        recipe match {
          case Some(r) =>
            parser.getSteps(r) match {
              case Right(steps) =>
                steps.zipWithIndex.foreach((step, i) => println(s"${i + 1}) $step"))
              case Left(error) => println(s"Ошибка при поиске рецепта: $error")
            }
          case None => println("Рецепт не найден")
        }
      case Left(error) => println(s"Ошибка при поиске рецепта: $error")
    }
  }

  def start(): Unit = {
    while(true) {
      printMenu()
      val option = readLine("Выберите опцию: ")
      option match {
        case "1" => addIngredient()
        case "2" => removeIngredient()
        case "3" => getRecipe()
        case "q" => return
        case _ => println("Некорректная опция")
      }
    }
  }
}
