package dev.yanovsky
package models

case class Ingredient(id: Int, name: String)
