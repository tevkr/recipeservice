package dev.yanovsky
package backend

import models.{Ingredient, Recipe}

import io.circe.Decoder
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.*
import sttp.client3.*
import sttp.client3.circe.asJson
import sttp.model.*
import sttp.client3.testing.*
import sttp.model.Uri.QuerySegment
import org.scalatest.matchers.should.Matchers
import sttp.model.Uri.*

import scala.util.{Failure, Success, Try}

class EdimDomaParserSpec extends AnyFlatSpec with Matchers {
  val baseUrl = "https://www.edimdoma.ru/retsepty"
  "getIngredients method" should "return a sequence of corresponding(suitable) ingredients" in {
    val testingBackend = SttpBackendStub.synchronous
      .whenRequestMatches(_.uri.querySegments.head == QuerySegment.KeyValue("q", "колбаса"))
      .thenRespond("[{\"id\":1922,\"title\":\"колбаса\",\"name\":\"колбаса\",\"validAmountTypes\"" +
        ":[18,10,30,17,7,4],\"unit_ids\":[18,10,30,17,7,4]},{\"id\":4852,\"title\":\"колбаса вареная\",\"" +
        "name\":\"колбаса вареная\",\"validAmountTypes\":[10,4],\"unit_ids\":[10,4]},{\"id\":7619,\"title\":\"колбаса варено-копченая\"" +
        ",\"name\":\"колбаса варено-копченая\",\"validAmountTypes\":[14,10,4],\"unit_ids\":[14,10,4]}]")
      .whenRequestMatches(_.uri.querySegments.head == QuerySegment.KeyValue("q", "сырокопченая"))
      .thenRespond("[{\"id\":1337,\"title\":\"ветчина сырокопченая\",\"name\":\"ветчина сырокопченая\",\"validAmountTypes\"" +
        ":[18,30,14,10,4],\"unit_ids\":[18,30,14,10,4]},{\"id\":7941,\"title\":\"колбаса сырокопченая\",\"name\":\"" +
        "колбаса сырокопченая\",\"validAmountTypes\":[18,10,4],\"unit_ids\":[18,10,4]}]")

    val expected1 = Right(Seq(Ingredient(1922,"колбаса"), Ingredient(4852,"колбаса вареная"), Ingredient(7619,"колбаса варено-копченая")))
    val expected2 = Right(Seq(Ingredient(1337,"ветчина сырокопченая"), Ingredient(7941,"колбаса сырокопченая")))
    val edimDomaParser = new EdimDomaParser(testingBackend)

    val result1 = edimDomaParser.getIngredients("колбаса")
    val result2 = edimDomaParser.getIngredients("сырокопченая")

    result1 shouldBe expected1
    result2 shouldBe expected2
  }

  it should "return error if bad response comes" in {
    val testingBackend = SttpBackendStub.synchronous
      .whenAnyRequest
      .thenRespond("adsfadfle\":\"ветчина сdasdasопченая\",\"name\":\"ветчина сырокопченая\",\"validAmountTypes\"" +
        ":[18,30,14,10,4],\"unit_ids\":[18,30,14,1asda")
    val edimDomaParser = new EdimDomaParser(testingBackend)
    val result = edimDomaParser.getIngredients("doesn't matter")
    result.left.toOption.exists(_.contains("expected json value")) shouldBe true
  }

  it should "return error if empty Seq of ingredients comes" in {
    val testingBackend = SttpBackendStub.synchronous
      .whenAnyRequest
      .thenRespond("[]")
    val edimDomaParser = new EdimDomaParser(testingBackend)
    val result = edimDomaParser.getIngredients("doesn't matter")
    result.left.toOption.exists(_.contains("Нет подходящих ингредиентов")) shouldBe true
  }

  "getRecipes method" should "return a sequence of corresponding recipes for seq of ingredients" in {
    val filePath1695 = s"${System.getProperty("user.dir")}/src/test/scala/backend/ParserCases/recipes-page-1695.html"
    val recipePage1695: Try[String] = Try {
      val source = scala.io.Source.fromFile(filePath1695, "utf-8")
      try source.mkString finally source.close()
    }
    val recipePage1695Content = recipePage1695 match {
      case Success(content) => content
      case Failure(exception) =>
        fail(s"Failed to read file: ${exception.getMessage}")
    }

    val filePath1974 = s"${System.getProperty("user.dir")}/src/test/scala/backend/ParserCases/recipes-page-1974.html"
    val recipePage1974: Try[String] = Try {
      val source = scala.io.Source.fromFile(filePath1974, "utf-8")
      try source.mkString finally source.close()
    }
    val recipePage1974Content = recipePage1974 match {
      case Success(content) => content
      case Failure(exception) =>
        fail(s"Failed to read file: ${exception.getMessage}")
    }

    val testingBackend = SttpBackendStub.synchronous
      .whenRequestMatches(_.uri.querySegments.head == Uri.QuerySegment.Value("with_ingredient[]=1695", QuerySegmentEncoding.RelaxedWithBrackets))
      .thenRespond(recipePage1695Content)
      .whenRequestMatches(_.uri.querySegments.head == Uri.QuerySegment.Value("with_ingredient[]=1974", QuerySegmentEncoding.RelaxedWithBrackets))
      .thenRespond(recipePage1974Content)

    val edimDomaParser = new EdimDomaParser(testingBackend)

    val expected1 = Right(Seq(Recipe("150989-kartoshka-s-myasom","Картошка с мясом"), Recipe("150982-tushenaya-kapusta-v-zalivke","Тушеная капуста в заливке"), Recipe("150975-zakuska-iz-krevetok-v-tartaletkah","Закуска из креветок в тарталетках"), Recipe("150956-perets-farshirovannyy-pshenom-s-pomidorami-i-shpinatom","Перец, фаршированный пшеном с помидорами и шпинатом"), Recipe("150962-salat-mimoza","Салат «Мимоза»"), Recipe("150948-pirozhki-iz-sloenogo-testa-s-myasom","Пирожки из слоеного теста с мясом"), Recipe("150896-pastushiy-pirog","Пастуший пирог"), Recipe("150895-bifshteks-s-yaytsom-i-poreem","Бифштекс с яйцом и пореем"), Recipe("150815-zharkoe-v-gorshochke","Жаркое в горшочке"), Recipe("150727-kartoshka-s-gribami-v-duhovke","Картошка с грибами в духовке"), Recipe("150708-zapechennyy-kartofel","Запеченный картофель"), Recipe("150705-salat-grozd-vinograda-s-kuritsey","Салат «Гроздь винограда» с курицей"), Recipe("150681-pide-s-farshem-i-tomatnym-sousom","Пиде с фаршем и томатным соусом"), Recipe("150674-pp-burger-s-kotletoy-iz-indeyki","ПП-бургер с котлетой из индейки"), Recipe("150634-lapshevnik-s-tvorogom-i-syrom","Лапшевник с творогом и сыром"), Recipe("150631-syrno-chesnochnaya-lepeshka","Сырно-чесночная лепешка"), Recipe("150612-salat-s-kuritsey-s-vinogradom","Салат с курицей с виноградом"), Recipe("150523-pirog-s-ryboy-i-yaytsom","Пирог с рыбой и яйцом"), Recipe("150503-tvorozhnye-bulochki-s-syrom","Творожные булочки с сыром"), Recipe("150323-kotlety-s-kabachkami","Котлеты с кабачками"), Recipe("150314-lepeshki-iz-tsukini-s-syrom","Лепешки из цукини с сыром"), Recipe("150230-zapekanka-s-kuritsey-i-tsvetnoy-kapustoy","Запеканка с курицей и цветной капустой"), Recipe("150221-kesadilya-s-kuritsey-i-ovoschami","Кесадилья с курицей и овощами"), Recipe("150210-blinchiki-na-risovoy-muke","Блинчики на рисовой муке"), Recipe("150189-losos-pod-ovoschnoy-shuboy","Лосось под овощной шубой"), Recipe("150141-yaichnyy-rulet-s-morkovyu","Яичный рулет с морковью"), Recipe("150119-myasnaya-zapekanka-s-kabachkom","Мясная запеканка с кабачком"), Recipe("150103-syrnye-vafli","Сырные вафли"), Recipe("150077-kukuruznyy-hleb-na-rzhanoy-zakvaske-i-ryazhenke","Кукурузный хлеб на ржаной закваске и ряженке"), Recipe("150053-zapekanka-iz-tsukini-s-farshem","Запеканка из цукини с фаршем"), Recipe("150044-tefteli-v-syrnoy-zalivke","Тефтели в сырной заливке"), Recipe("150007-salat-s-zelenoy-fasolyu-i-svekloy","Салат с зеленой фасолью и свеклой"), Recipe("149989-grechka-zapechennaya-so-svininoy","Гречка, запеченная со свининой"), Recipe("149974-koreyskiy-ulichnyy-sendvich-so-svezhimi-ovoschami","Корейский уличный сэндвич со свежими овощами"), Recipe("149977-zapekanka-iz-kabachkov-s-syrom","Запеканка из кабачков с сыром"), Recipe("149957-zapekanka-iz-indeyki-s-kapustoy-i-syrom","Запеканка из индейки с капустой и сыром"), Recipe("149940-sufle-kurinoe","Суфле куриное"), Recipe("149891-pechenye-pomidory","Печеные помидоры"), Recipe("149875-salat-nezhnost","Салат «Нежность»"), Recipe("149847-bantiki-s-kuritsey-shpinatom-i-kedrovymi-oreshkami","Бантики с курицей, шпинатом и кедровыми орешками"), Recipe("149750-buterbrody-so-shprotami-i-syrom","Бутерброды со шпротами и сыром"), Recipe("149741-zrazy-kartofelnye-s-gribami","Зразы картофельные с грибами"), Recipe("149617-serdechki-zapechennye-s-yablokom-lukom-i-syrom","Сердечки, запеченные с яблоком, луком и сыром"), Recipe("149688-kapustnik","Капустник"), Recipe("149670-kotlety-iz-svininy-s-gerkulesom","Котлеты из свинины с геркулесом"), Recipe("149545-syrno-krabovyy-salat","Сырно-крабовый салат"), Recipe("149455-syrno-molochnye-kotlety-iz-kurinoy-grudki","Сырно-молочные котлеты из куриной грудки"), Recipe("149437-kapustnaya-zapekanka-s-myasom-i-gribami","Капустная запеканка с мясом и грибами"), Recipe("149390-svinina-pod-krabovoy-shuboy","Свинина под крабовой шубой"), Recipe("149383-kroshka-kartoshka-s-bekonom-i-syrom","Крошка-картошка с беконом и сыром"), Recipe("149370-kartoshka-k-uzhinu","Картошка к ужину"), Recipe("149295-lenivyy-hachapuri","Ленивый хачапури"), Recipe("149231-pirog-s-syrom","Пирог с сыром"), Recipe("149002-bliny-s-kartofelem-syrom-i-zelenyu","Блины с картофелем, сыром и зеленью"), Recipe("149163-kukuruznye-krekery-s-syrom","Кукурузные крекеры с сыром")))
    val expected2 = Right(Seq(Recipe("147484-tost-k-zavtraku-s-yaytsom","Тост к завтраку с яйцом"), Recipe("146911-kuritsa-tomlennaya-s-batatom-i-domashney-kolbasoy","Курица, томленная с бататом и домашней колбасой"), Recipe("145944-retsept-okroshki-so-smetanoy-i-uksusom","Рецепт окрошки со сметаной и уксусом"), Recipe("144943-mini-pitstsy-na-skovorode","Мини-пиццы на сковороде"), Recipe("143501-kartofel-s-nachinkoy","Картофель с начинкой"), Recipe("143493-venok-s-syrom-i-kolbasoy","Венок с сыром и колбасой"), Recipe("142408-pitstsa-s-tomatami-i-kolbasoy","Пицца с томатами и колбасой"), Recipe("141251-syrnye-yaytsa-po-angliyski","Сырные яйца по-английски"), Recipe("141220-salat-s-kozim-syrom-inzhirom-i-syrovyalenym-myasom","Салат с козьим сыром, инжиром и сыровяленым мясом"), Recipe("139432-zakusochnye-ruletiki","Закусочные рулетики"), Recipe("138238-solyanka-by-alekseev","Солянка by Alekseev"), Recipe("133958-bystraya-pitstsa-na-lavashe","Быстрая пицца на лаваше"), Recipe("133625-salat-so-shprotami-myshonok","Салат со шпротами «Мышонок»"), Recipe("129726-pitstsa-po-holostyatski","Пицца по-холостяцки"), Recipe("125324-bystraya-pitstsa","Быстрая пицца"), Recipe("121835-sbornaya-pitstsa-na-medovom-teste","Сборная пицца на медовом тесте"), Recipe("120149-italyanskaya-pitstsa","Итальянская пицца"), Recipe("115666-duhovye-pirozhki-s-myasom","Духовые пирожки с мясом"), Recipe("115421-pitstsa","Пицца"), Recipe("112711-kanape-domino","Канапе «Домино»"), Recipe("110076-smazhenka-s-kolbasoy-i-mayonezom","Смаженка с колбасой и майонезом"), Recipe("104609-zapekanka-posleprazdnichnaya","Запеканка «Послепраздничная»"), Recipe("98509-farshirovannye-yaytsa-v-suharyah","Фаршированные яйца в сухарях"), Recipe("92780-mini-pitstsy","Мини-пиццы"), Recipe("81416-mnogosloynaya-ekspress-pitstsa-na-skovorode","Многослойная экспресс-пицца на сковороде"), Recipe("80994-letnyaya-pitstsa","Летняя пицца"), Recipe("80918-teplyy-salat-iz-pasty-i-ovoschey","Теплый салат из пасты и овощей"), Recipe("77885-ekspress-zavtrak","Экспресс завтрак"), Recipe("77844-pirog-k-zavtraku","Пирог к завтраку"), Recipe("76349-pitstsa-domashnyaya-s-kolbasoy","Пицца \"Домашняя с колбасой\""), Recipe("74603-dzhambalayya-iz-nyu-orleana","Джамбалайя из Нью-Орлеана"), Recipe("71326-salat-zhele-olivie","Салат-желе \"Оливье\""), Recipe("69427-salat-iz-pasty-s-fasolyu-i-kukuruzoy","Салат из пасты с фасолью и кукурузой"), Recipe("64703-goryachiy-buterbrod-s-kolbasoy-i-smetannym-sousom","Горячий бутерброд с колбасой и сметанным соусом"), Recipe("63952-frittata-iz-zamorozhennyh-ovoschey","Фриттата из замороженных овощей"), Recipe("63072-salat-lunnyy-svet","Салат «Лунный свет»"), Recipe("62943-salat-shikarnyy","Салат \"Шикарный\""), Recipe("62630-zapechennyy-farshirovannyy-kartofel-ot-babushki-franki","Запеченный фаршированный картофель от бабушки Франки"), Recipe("61755-retsept-pitstsy-s-kolbasoy-i-gribami","Рецепт пиццы с колбасой и грибами"), Recipe("61548-sendvichi-kotyata","Сэндвичи \"Котята\""), Recipe("57843-pitstsa-po-bystromu","Пицца по-быстрому"), Recipe("60464-kartofelnye-oladi-s-morkovyu-i-kolbasoy","Картофельные оладьи с морковью и колбасой"), Recipe("60226-hlebnye-trubochki-s-nachinkoy","Хлебные трубочки с начинкой"), Recipe("59652-syrno-tykvennye-lepeshki-s-kolbasoy-i-zelenyu","Сырно-тыквенные лепешки с колбасой и зеленью"), Recipe("58979-pitstsa-chertovka","Пицца \"Чертовка\""), Recipe("58644-polpettone-iz-kuritsy","Полпеттоне из курицы"), Recipe("58568-bystraya-pitstsa","Быстрая пицца"), Recipe("58424-otkrytyy-derevenskiy-pirog-s-syrom-dzhyugas","Открытый деревенский пирог с сыром Джюгас"), Recipe("58312-salat-yaponsko-russkiy-fyuzhn","Салат \"Японско-Русский Фьюжн\""), Recipe("58074-pitstsa-kaltsone-s-gribami-ovoschami-i-syrom-dzhyugas","Пицца кальцоне с грибами, овощами и сыром Джюгас"), Recipe("58035-machanka-po-krestyanski-s-blinami","Мачанка по-крестьянски с блинами"), Recipe("57228-zharenye-makarony-s-yaytsami-syrom-kolbasoy-i-pomidorami","Жареные макароны с яйцами, сыром, колбасой и помидорами"), Recipe("56922-italyanskie-teftelki","Итальянские тефтельки"), Recipe("57150-salat-dlya-marii","Салат для Марии"), Recipe("57126-tatarskie-buterbrody","\"Татарские\" Бутерброды")))
    val result1 = edimDomaParser.getRecipes(Seq(Ingredient(1695, "сыр")))
    val result2 = edimDomaParser.getRecipes(Seq(Ingredient(1974, "колбаса")))

    result1 shouldBe expected1
    result2 shouldBe expected2
  }

  it should "return error if bad response comes" in {
    val testingBackend = SttpBackendStub.synchronous
      .whenAnyRequest
      .thenRespondServerError()
    val edimDomaParser = new EdimDomaParser(testingBackend)
    val result = edimDomaParser.getRecipes(Seq(Ingredient(1337, "doesnt matter")))
    result shouldBe Left("Internal server error")
  }

  "getSteps method" should "return a sequence of strings containing steps for recipe" in {
    val filePath150708 = s"${System.getProperty("user.dir")}/src/test/scala/backend/ParserCases/recipe-page-150708.html"
    val recipePage150708: Try[String] = Try {
      val source = scala.io.Source.fromFile(filePath150708, "utf-8")
      try source.mkString finally source.close()
    }
    val recipePage150708Content = recipePage150708 match {
      case Success(content) => content
      case Failure(exception) =>
        fail(s"Failed to read file: ${exception.getMessage}")
    }

    val filePath149957 = s"${System.getProperty("user.dir")}/src/test/scala/backend/ParserCases/recipes-page-149957.html"
    val recipePage149957: Try[String] = Try {
      val source = scala.io.Source.fromFile(filePath149957, "utf-8")
      try source.mkString finally source.close()
    }
    val recipePage149957Content = recipePage149957 match {
      case Success(content) => content
      case Failure(exception) =>
        fail(s"Failed to read file: ${exception.getMessage}")
    }

    val testingBackend = SttpBackendStub.synchronous
      .whenRequestMatches(_.uri.path.contains("150708-zapechennyy-kartofel"))
      .thenRespond(recipePage150708Content)
      .whenRequestMatches(_.uri.path.contains("149957-zapekanka-iz-indeyki-s-kapustoy-i-syrom"))
      .thenRespond(recipePage149957Content)

    val edimDomaParser = new EdimDomaParser(testingBackend)

    val expected1 = Right(Seq("Промыть картофель и разрезать поперек гармошкой не до конца. Наполнить разрезы кусочками холодного масла и тертым чесноком: в одном кармашке будет масло, в другом чеснок.",
      "Переложить картофель в форму, застеленную пергаментом. Посолить и отправить в разогретую духовку на 1 час при 180°C. Пока печется картофель, нарезать тонкими ломтиками ветчину и обжарить на сливочном масле.",
      "Достать картофель и положить в кармашки по ломтику сыра. Отправить в духовку еще на 5 минут. На готовый картофель выложить творожный сыр, сверху — ветчину и посыпать зеленым луком."))
    val expected2 = Right(Seq("Капусту нашинковать и слегка перетереть руками с солью.",
      "Лук нарезать мелкими кубиками и потушить на растительном масле до золотистого цвета. Добавить капусту, специи и томить под крышкой на медленном огне минут 10. Убрать с плиты и дать остыть.",
      "К фаршу добавить измельченный в блендере лук, горчицу, яйцо, специи. Хорошо перемешать.",
      "Форму для запекания смазать маслом. Фарш разделить на 3 части, а капусту на две. В форму выложить фарш, затем капусту, снова фарш, капусту и сверху фарш. Накрыть фольгой и запекать при температуре 200°C 35 минут.",
      "Убрать фольгу и выложить сверху пластинки сыра. Продолжить запекать до золотистой сырной корочки.",
      "Подавать блюдо теплым со свежими овощами."))
    val result1 = edimDomaParser.getSteps(Recipe("150708-zapechennyy-kartofel","Запеченный картофель"))
    val result2 = edimDomaParser.getSteps(Recipe("149957-zapekanka-iz-indeyki-s-kapustoy-i-syrom", "Запеканка из индейки с капустой и сыром"))

    result1 shouldBe expected1
    result2 shouldBe expected2
  }

  it should "return error if bad response comes" in {
    val testingBackend = SttpBackendStub.synchronous
      .whenAnyRequest
      .thenRespondServerError()
    val edimDomaParser = new EdimDomaParser(testingBackend)
    val result = edimDomaParser.getSteps(Recipe("doesn't matter", "doesn't matter"))
    result shouldBe Left("Internal server error")
  }

}
