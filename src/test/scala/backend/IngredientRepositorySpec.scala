package dev.yanovsky
package backend

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class IngredientRepositorySpec  extends AnyFlatSpec with Matchers with IngredientRepositoryBehaviors {
  behavior of "In memory ingredient repository"
  it should behave like validIngredientRepository(
    new InMemoryIngredientRepository()
  )
}
