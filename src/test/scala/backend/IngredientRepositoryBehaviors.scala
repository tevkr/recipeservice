package dev.yanovsky
package backend

import dev.yanovsky.models.Ingredient
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


trait IngredientRepositoryBehaviors {
  this: AnyFlatSpec with Matchers =>
  def validIngredientRepository(testedRepository: => IngredientRepository): Unit = {
    trait TestWiring {
      val repository: IngredientRepository = testedRepository
    }

    it should "return message while trying getting all ingredients from empty repository" in new TestWiring {
      repository.getAll() shouldBe Left("Нет ингредиентов")
    }

    it should "return all ingredients" in new TestWiring {
      repository.add(Ingredient(1, "name1"))
      repository.add(Ingredient(2, "name2"))
      repository.getAll() shouldBe Right(Seq(Ingredient(1, "name1"), Ingredient(2, "name2")))
    }

    it should "add ingredient" in new TestWiring {
      repository.add(Ingredient(1, "name1"))
      repository.getAll() shouldBe Right(Seq(Ingredient(1, "name1")))
    }

    it should "return message while trying to add repetitive ingredient" in new TestWiring {
      repository.add(Ingredient(1, "name1"))
      repository.add(Ingredient(1, "name1")) shouldBe Left("Ингредиент с именем name1 уже существует")
    }

    it should "delete ingredient by name" in new TestWiring {
      repository.add(Ingredient(1, "name1"))
      repository.add(Ingredient(2, "name2"))
      repository.deleteByName("name1")
      repository.getAll() shouldBe Right(Seq(Ingredient(2, "name2")))
    }

    it should "return message while trying to delete non-existent ingredient" in new TestWiring {
      repository.deleteByName("name") shouldBe Left(s"Ингредиент с именем name не найден")
    }
  }
}
