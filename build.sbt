ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.2.0"

lazy val root = (project in file("."))
  .settings(
    name := "BA2022-coursework",
    idePackagePrefix := Some("dev.yanovsky"),
    libraryDependencies ++= Seq(
      "com.softwaremill.sttp.client3" %% "core" % "3.8.5",
      "com.softwaremill.sttp.client3" %% "zio" % "3.8.5",
      "com.softwaremill.sttp.client3" %% "circe" % "3.8.5",
      "org.jsoup" % "jsoup" % "1.15.3",
      "io.circe" %% "circe-generic" % "0.14.1",
      "io.circe" %% "circe-parser" % "0.14.1",
      "org.http4s" %% "http4s-ember-server" % "0.23.16",
      "org.scalatest" %% "scalatest" % "3.2.14" % Test,
      "org.mockito" % "mockito-core" % "4.8.0" % Test
    )
  )
